package ch.ffhs.ftoop.p1.deadlock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Aufgabe: Dieses Programm demonstriert einen Deadlock. Lassen Sie dieses
 * Programm mehrfach laufen und schauen Sie, was passiert.
 * 
 * a) Erklären Sie das Verhalten des Programms. b) Korrigieren Sie das Programm,
 * so dass es sich korrekt verhält. Verändern Sie dabei nicht die Klasse Friend.
 * 
 * Antwort: a) Der Thread "gastonThread" ruft die Methode bow mit dem Lock des Objekts alphonse auf. 
 * Parallel zum Aufruf des Threads gastonThread ruft der andere Thread "alphonseThread" auch die bow Methode auf jedoch mit dem Lock des "gaston" Objekts. 
 * In der bow Methode rufen dann beide Threads die Methode bowBack mit dem Lock des jeweils anderen Objekts auf. (Z.B ruft der eine Thread in der bow Methode, welches mit dem gaston Objekt synchronisiert ist die reBow Methode auf welche mit dem "alphons" Objekt synchronisiert ist und umgekehrt.) 
 * Da jetzt paralell beide Threads versuchen auf eine synchronized Methode des jeweilig anderen Lock Objekts zugreifen, entsteht ein Deadlock.
 * b) Ich habe das Programm mit einem Lock versehen, welches den gesamten Durchlauf eines Threads durch die Methoden bow und dann bowBack erlaubt, ohne vom anderen Thread
 * "gestört" zu werden. Wenn der Thread die Methoden durchlaufen hat, gibt er das Lock wieder ab und der andere Thread kann die Methoden durchlaufen (auch wieder mit dem Lock 
 * versehen).
 * 
 * @author bele
 * 
 */
public class Deadlock {
	private final Lock lock = new ReentrantLock();
	/**
	 * Creates two new Friend Objects named Gaston and Alphonse. Then generates two Threads, which are using methods from the class Friend.
	 * 
	 * 
	 * @throws InterruptedException
	 */
	void doStuff() throws InterruptedException {
		final Friend alphonse = new Friend("Alphonse");
		final Friend gaston = new Friend("Gaston");
		
		Thread gastonThread = new Thread(new Runnable() {
			public void run() {
				lock.lock();		//setze ein lock
				try {
					alphonse.bow(gaston);	
				}
				finally {
					lock.unlock();	//unlock
				}
				
			}
		}, "Gaston");
		gastonThread.start();

		Thread alphonseThread = new Thread(new Runnable() {
			public void run() {
				lock.lock();
				try {
					gaston.bow(alphonse);	
				}
				finally {
					lock.unlock();
				}
			}
		}, "Alphonse");
		alphonseThread.start();

		alphonseThread.join();
		gastonThread.join();

	}
	
	/**
	 * Creates a new Deadlock object and calls the method doStuff on it.
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		Deadlock d = new Deadlock();
		d.doStuff();
	}
}

class Friend {
	private final String name;

	public Friend(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public synchronized void bow(Friend bower) {
		
			System.out.format("%s: %s" + "  has bowed to me!%n", this.name,
					bower.getName());
			bower.bowBack(this);
	
		
	}

	public synchronized void bowBack(Friend bower) {
		System.out.format("%s: %s" + " has bowed back to me!%n", this.name,
				bower.getName());
	}
}
