package ch.ffhs.ftoop.p1.deadlock;

import org.junit.Test;

import student.TestCase;

public class DeadlockTest extends TestCase {

	public void testDoStuff() throws InterruptedException {

		for (int i = 0; i < 10; i++) {
			Deadlock d = new Deadlock();
			d.doStuff();
		}

	}
	
	public void testBow() {
		Friend f1 = new Friend("Fritz");
		Friend f2 = new Friend("Sepp");
		f1.bow(f2);
		assertFuzzyEquals(
				"Fritz: Sepp has bowed to me!\nSepp: Fritz has bowed back to me!",
				systemOut().getHistory());
	}
	public void testReBow() {
		Friend f1 = new Friend("Fritz");
		Friend f2 = new Friend("Sepp");
		f1.bowBack(f2);
		assertFuzzyEquals(
				"Fritz: Sepp has bowed back to me!",
		systemOut().getHistory());
	}

}

